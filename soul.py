from enum import Enum
import sys
import random

motive = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
old_motive = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

clock_h = 8
clock_m = 0
clock_d = 1

class game(Enum):
    mHappyLife = 0
    mHappyWeek = 1
    mHappyDay = 2
    mHappyNow = 3
    mPhysical = 4
    mEnergy = 5
    mComfort = 6
    mHunger = 7
    mHygiene = 8
    mBladder = 9
    mMental = 10
    mAlertness = 11
    mStress = 12
    mEnvironment = 13
    mSocial = 14
    mEntertained = 15

DAYTICKS = 720 # 1 tick = 2 minutes game time
WEEKTICKS = 5040

def say(msg):
    print(f'[Day {clock_d}, {clock_h:02d}:{clock_m:02d}] {msg}')
    #print(motive)

def sim_motives(count):
    tem = 0.0
    z = 0
    r = {100,100,140,140} # Rect r = {100,100,140,140}
    
    global clock_m, clock_h, clock_d
    clock_m += 2
    if clock_m > 58:
        clock_m = 0
        clock_h += 1
        if clock_h > 24:
            clock_h = 1
            clock_d += 1
    
    if clock_d == 365:
        say('This is the end!')
        sys.exit(1)


    # energy
    if motive[game.mEnergy.value] > 0:
        if motive[game.mAlertness.value] > 0:
            motive[game.mEnergy.value] -= (motive[game.mAlertness.value]/100)
        else:
            motive[game.mEnergy.value] -= (motive[game.mAlertness.value]/100) * ((100 + motive[game.mEnergy.value]) / 50)
    else:
        if motive[game.mAlertness.value] > 0:
            motive[game.mEnergy.value] -= (motive[game.mAlertness.value] / 100) * ((100 + motive[game.mEnergy.value]) / 50)
        else:
            motive[game.mEnergy.value] -= (motive[game.mAlertness.value] / 100)

    if motive[game.mHunger.value] > old_motive[game.mHunger.value]: # I had some food
        tem = motive[game.mHunger.value] - old_motive[game.mHunger.value]
        motive[game.mEnergy.value] += tem / 4

    # comfort
    if motive[game.mBladder.value] < 0:
        motive[game.mComfort.value] =+ motive[game.mBladder.value] / 10 # max -10

    if motive[game.mHygiene.value] < 0:
        motive[game.mComfort.value] += motive[game.mHygiene.value] / 20 # max -5

    if motive[game.mHunger.value] < 0:
        motive[game.mComfort.value] += motive[game.mHunger.value] / 20 # max -5

    # dec a max 100/cycle in a cubed curve (seek zero)
    motive[game.mComfort.value] -= (motive[game.mComfort.value] * motive[game.mComfort.value] * motive[game.mComfort.value]) / 10000

    # hunger
    tem = ((motive[game.mAlertness.value] + 100)/200) * ((motive[game.mHunger.value] + 100)/100) # // ^alert * hunger^0
    motive[game.mHunger.value] -= tem
    
    if motive[game.mStress.value] < 0: # stress -> hunger        
        motive[game.mHunger.value] =+ (motive[game.mStress.value] / 100) * ((motive[game.mHunger.value] + 100)/100)
    
    if motive[game.mHunger.value] < -99:
        say('You have starved to death')
        sys.exit(1)
        motive[game.mHunger.value] = 80

    # hygiene
    if motive[game.mAlertness.value] > 0:
        motive[game.mHygiene.value] -= .3
    else:
        motive[game.mHygiene.value] -= .1
    
    if motive[game.mHygiene.value] < -97: # hit limit, bath
        say('You smell very bad, mandatory bath.')
        motive[game.mHygiene.value] = 80
    
    # bladder
    if motive[game.mAlertness.value] > 0: # bladder fills faster while awake
        motive[game.mHygiene.value] -= .4
    else:
        motive[game.mHygiene.value] -= .2
    
    if motive[game.mHunger.value] > old_motive[game.mHunger.value]: # food eaten goes into bladder
        tem = motive[game.mHunger.value] - old_motive[game.mHunger.value]
        motive[game.mBladder.value] -= tem / 4
    
    if motive[game.mBladder.value] < -97: # hit limit, gotta go
        if motive[game.mAlertness.value] < 0:
            say('You have wet the bed')
        else:
            say('You have soiled the carpet')
        motive[game.mBladder.value] = 90;

    # alertness
    if motive[game.mAlertness.value] > 0:
        tem = (100 - motive[game.mAlertness.value]) / 50 # max delta at zero
    else:
        tem = (motive[game.mAlertness.value] + 100) / 50

    if motive[game.mEnergy.value] > 0:
        if motive[game.mAlertness.value] > 0:
            motive[game.mAlertness.value] += (motive[game.mEnergy.value] / 100) * tem
        else:
            motive[game.mAlertness.value] += (motive[game.mEnergy.value] / 100)
    else:
        if motive[game.mAlertness.value] > 0:
            motive[game.mAlertness.value] += (motive[game.mEnergy.value] / 100)
        else:
            motive[game.mAlertness.value] += (motive[game.mEnergy.value] /100) * tem

    motive[game.mAlertness.value] += (motive[game.mEntertained.value] / 300) * tem

    if motive[game.mBladder.value] < -50:
        motive[game.mAlertness.value] -= (motive[game.mBladder.value] / 100) * tem

    # stress
    motive[game.mStress.value] += motive[game.mComfort.value] / 10      # max -10
    motive[game.mStress.value] += motive[game.mEntertained.value] / 10  # max -10
    motive[game.mStress.value] += motive[game.mEnvironment.value] / 15  # max -7
    motive[game.mStress.value] += motive[game.mSocial.value] / 20       # max -5

    if motive[game.mAlertness.value] < 0: # Cut stress while asleep
        motive[game.mStress.value] = motive[game.mStress.value] / 3

    # dec a max 100/cycle in a cubed curve (seek zero)
    motive[game.mStress.value] -= (motive[game.mStress.value] * motive[game.mStress.value] * motive[game.mStress.value]) / 10000;

    if motive[game.mStress.value] < 0:
        if (random.randrange(30) - 100) > motive[game.mStress.value]:
            if (random.randrange(30) - 100) > motive[game.mStress.value]:
                say("You have lost your temper")
                change_motive(game.mStress, 20)

    # environment

    # social

    # entertained
    if motive[game.mAlertness.value] < 0: # cCut entertained while asleep
        motive[game.mEntertained.value] = motive[game.mEntertained.value] / 2

    # calc physical
    tem = motive[game.mEnergy.value]
    tem += motive[game.mComfort.value]
    tem += motive[game.mHunger.value]
    tem += motive[game.mHygiene.value]
    tem += motive[game.mBladder.value]
    tem = tem / 5

    if tem > 0: # map the linear average into squared curve
        tem = 100 - tem
        tem = (tem * tem) / 100
        tem = 100 - tem
    else:
        tem = 100 + tem
        tem = (tem * tem) / 100
        tem = tem - 100

    motive[game.mPhysical.value] = tem

    # calc mental
    tem = motive[game.mStress.value]
    tem += motive[game.mStress.value]
    tem += motive[game.mEnvironment.value]
    tem += motive[game.mSocial.value]
    tem += motive[game.mEntertained.value]
    tem = tem / 5

    if tem > 0: # map the linear average into squared curve
        tem = 100 - tem
        tem = (tem * tem) / 100
        tem = 100 - tem
    else:
        tem = 100 + tem
        tem = (tem * tem) / 100
        tem = tem - 100

    motive[game.mMental.value] = tem

    # calc and average happiness
    # happy = mental + physical
    motive[game.mHappyNow.value] = (motive[game.mPhysical.value]+motive[game.mMental.value]) / 2
    motive[game.mHappyDay.value] = ((motive[game.mHappyDay.value] * (DAYTICKS-1)) + motive[game.mHappyNow.value]) / DAYTICKS
    motive[game.mHappyWeek.value] = ((motive[game.mHappyWeek.value] * (WEEKTICKS-1)) + motive[game.mHappyNow.value] / WEEKTICKS)
    motive[game.mHappyLife.value] = ((motive[game.mHappyLife.value] * 9) + motive[game.mHappyWeek.value]) / 10

    for z in list(game):
        if motive[z.value] > 100: # Check for over/under flow
            motive[z.value] = 100
        if motive[z.value] < -100:
            motive[z.value] = -100
        old_motive[z.value] = motive[z.value] # Save set in old_motives (for delta tests)


def change_motive(some_motive, value): # Currently broken
    return
    motive[game.a_motive.value] += value
    if motive[game.a_motive.value] > 100:
        motive[game.a_motive.value] = 100
    if motive[game.a_motive.value] < -100:
        motive[game.a_motive.value] = -100

def sim_job(type):
    clock_h += 9
    if clock_h > 24:
        clock_h -= 24
    motive[game.mEnergy.value] = ((motive[game.mEnergy.value] + 100) & .3) - 100
    motive[game.mHunger.value] = -60 + random.random(20)
    motive[game.mHygiene.value] = -70 + random.random(30)
    motive[game.mBladder.value] = -50 + random.random(50)
    motive[game.mAlertness.value] = 10 + random.random(10)
    motive[game.mStress.value] = -50 + random.random(50)

def adjust_motives(x, y):
    pass

def draw_motive_sheet():
    pass

def draw_motive(xpos, ypos, value):
    pass

def init_motives():
    count = 0
    while count < 16:
        motive[count] = 0
        count += 1
    motive[game.mEnergy.value] = 70
    motive[game.mAlertness.value] = 20
    motive[game.mHunger.value] = -40

if __name__ == '__main__':
    init_motives()
    while True:
        sim_motives(5)